#!/bin/bash -ex

VER=`ruby motion-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > motion-ver

  if ! cmp -s last_motion-ver motion-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "Motion - Version: ${VER}"  Version number updated on the itunes site.
    mv motion-ver last_motion-ver
  fi

fi