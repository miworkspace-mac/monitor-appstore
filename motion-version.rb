#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

begin
  doc = Nokogiri::HTML(open("https://www.apple.com/final-cut-pro/motion/",'Accept-Language' => 'en', "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15"))
rescue
  puts "Failed to fetch download page"
  exit 1
end

link = doc.xpath("//a").find do |link|
  link.text.match(/Buy Motion/)
end

begin
  doca = Nokogiri::HTML(open("#{link['href']}",'Accept-Language' => 'en', "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download page"
  exit 1
end

linkver = doca.xpath("//p").find do |link2|
  link2.text.match(/Version/)
end

if linkver
  puts linkver.text
end