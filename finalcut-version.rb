#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

begin
  doc = Nokogiri::HTML(open("https://www.apple.com/final-cut-pro/", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download Original Final Cut page"
  exit 1
end

link = doc.xpath('//a').find {|x| x.inner_html.match(/Buy/) }

begin
  doca = Nokogiri::HTML(open("#{link['href']}", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download page"
  exit 1
end

linkver = doca.xpath('//*[@class="version-history__item__version-number"]').find {|x| x.inner_text.match(/.*/)}.text

puts linkver

#http://itunes.apple.com/us/app/final-cut-pro/id424389933?mt=12