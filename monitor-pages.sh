#!/bin/bash -ex

VER=`ruby pages-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > pages-ver

  if ! cmp -s last_pages-ver pages-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "Pages - Version: ${VER}"  Version number updated on the itunes site.
    mv pages-ver last_pages-ver
  fi

fi