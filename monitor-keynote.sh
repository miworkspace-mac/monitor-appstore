#!/bin/bash -ex

VER=`ruby keynote-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > keynote-ver

  if ! cmp -s last_keynote-ver keynote-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "Keynote - Version: ${VER}"  Version number updated on the itunes site.
    mv keynote-ver last_keynote-ver
  fi

fi