#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

begin
  doc = Nokogiri::HTML(open("https://www.apple.com/keynote/",'Accept-Language' => 'en', "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download Original Keynote page"
  exit 1
end

link = doc.xpath("//a").find do |link|
  link.text.match(/Download Keynote for Mac/)
end

begin
  doca = Nokogiri::HTML(open("#{link['href']}",'Accept-Language' => 'en', "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download page"
  exit 1
end

linkver = doca.xpath("//p").find do |link2|
  link2.text.match(/Version/)
end

if linkver
  puts linkver.text
end