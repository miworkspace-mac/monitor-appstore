#!/bin/bash -ex

VER=`ruby logicx-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > logicx-ver

  if ! cmp -s last_logicx-ver logicx-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "LogicX - Version: ${VER}"  Version number updated on the itunes site.
    mv logicx-ver last_logicx-ver
  fi

fi