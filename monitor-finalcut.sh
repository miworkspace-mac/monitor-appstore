#!/bin/bash -ex

VER=`ruby finalcut-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > finalcut-ver

  if ! cmp -s last_finalcut-ver finalcut-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "FinalCut - Version: ${VER}"  Version number updated on the itunes site.
    mv finalcut-ver last_finalcut-ver
  fi

fi