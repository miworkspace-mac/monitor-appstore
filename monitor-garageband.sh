#!/bin/bash -ex

VER=`ruby garageband-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > garageband-ver

  if ! cmp -s last_garageband-ver garageband-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "GarageBand - Version: ${VER}"  Version number updated on the itunes site.
    mv garageband-ver last_garageband-ver
  fi

fi