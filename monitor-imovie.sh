#!/bin/bash -ex

VER=`ruby imovie-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > imovie-ver

  if ! cmp -s last_imovie-ver imovie-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "iMovie - Version: ${VER}"  Version number updated on the itunes site.
    mv imovie-ver last_imovie-ver
  fi

fi