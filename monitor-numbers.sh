#!/bin/bash -ex

VER=`ruby numbers-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > numbers-ver

  if ! cmp -s last_numbers-ver numbers-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "Numbers - Version: ${VER}"  Version number updated on the itunes site.
    mv numbers-ver last_numbers-ver
  fi

fi