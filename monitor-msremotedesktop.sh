#!/bin/bash -ex

VER=`ruby msremotedesktop-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > msremotedesktop-ver

  if ! cmp -s last_msremotedesktop-ver msremotedesktop-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "MS Remote Desktop - Version: ${VER}"  Version number updated on the itunes site.
    mv msremotedesktop-ver last_msremotedesktop-ver
  fi

fi