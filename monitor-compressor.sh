#!/bin/bash -ex

VER=`ruby compressor-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > compressor-ver

  if ! cmp -s last_compressor-ver compressor-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "Compressor - Version: ${VER}"  Version number updated on the itunes site.
    mv compressor-ver last_compressor-ver
  fi

fi