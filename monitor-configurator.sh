#!/bin/bash -ex

VER=`ruby configurator-version.rb`

if [ "x${VER}" != "x" ]; then
  echo VER: "${VER}"
  echo "${VER}" > configurator-ver

  if ! cmp -s last_configurator-ver configurator-ver ; then
    ~/jenkins-trello/trello-notify.rb "To Do" "Configurator - Version: ${VER}"  Version number updated on the itunes site.
    mv configurator-ver configurator-ver
  fi

fi