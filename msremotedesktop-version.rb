#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

begin
  doc = Nokogiri::HTML(open("https://itunes.apple.com/us/app/microsoft-remote-desktop-10/id1295203466", 'Accept-Language' => 'en', "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download page"
  exit 1
end

linkver = doc.xpath("//p").find do |link2|
  link2.text.match(/Version/)
end

if linkver
  puts linkver.text
end
